<?php
require('animal.php');
require('Ape.php');
require('Frog.php');


$sheep = new animal("shaun");
echo "Nama Binatang : $sheep->name <br>"; // "shaun"
echo "Jumlah Kaki : $sheep->legs <br>"; // 2
echo "Jenis Berdarah Dingin : $sheep->cold_blooded <br><br>"; // false


$sungokong = new Ape("kera sakti");
echo "Nama Binatang : $sungokong->name <br>"; 
echo "Jumlah Kaki : $sungokong->legs <br>"; 
echo "Jenis Berdarah Dingin : $sungokong->cold_blooded <br>"; 
echo "Bunyi Teriakan : $sungokong->yell <br><br>";

$kodok = new Frog("buduk");
echo "Nama Binatang : $kodok->name <br>"; 
echo "Jumlah Kaki : $kodok->legs <br>"; 
echo "Jenis Berdarah Dingin : $kodok->cold_blooded <br>"; 
echo "Bunyi Lompatan : $kodok->jump <br><br>";